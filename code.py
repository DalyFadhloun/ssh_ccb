from fabric import Connection

# Replace these variables with your SSH server details
hostname = '192.168.0.166'
username = 'parallels'
password = '**********'

try:
    # Create an SSH connection
    c = Connection(host=hostname, user=username, connect_kwargs={"password": password})

    # Run the df -h command remotely
    result = c.run('df -h', hide=True)  # hide=True suppresses command output

    # Print the command result
    print(result.stdout)

except Exception as e:
    print(f"An error occurred: {str(e)}")
finally:
    try:
        # Close the SSH connection
        c.close()
    except Exception as e:
        print(f"An error occurred while closing the SSH connection: {str(e)}")
